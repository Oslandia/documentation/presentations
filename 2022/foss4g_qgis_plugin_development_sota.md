---
title: Better productivity for QGIS plugin developers
author: Julien Moura
date: 2022-08-26
tags:
  - cookiecutter
  - FOSS4G
  - plugin
  - QGIS
  - templater
  - tooling
type: slide
slideOptions:
  theme: oslandia
  transition: 'fade'
---

# QGIS Plugin Templater

> Better productivity for QGIS plugin developers  
> (or YAQPG, Yet Another Plugin Generator)

<table>
  <tr>
    <td>
        <img height=100px src="../_assets/oslandia_logo_rectangle.svg" />
      </td>
      <td>
        <img height=100px src="../_assets/2022_FOSS4G_Firenze/logo_foss4g_2022.png" />
    </td>
  </tr>
</table>

_FOSS4G 2022 (Firenze) - Talk (20 minutes)_

---

## Who's talking to you?

Julien Moura, GIS Consultant @ Oslandia ()...  
and also lead entertainer of [Geotribu.fr](http://geotribu.fr/)

<table>
  <tr>
    <td>
        <img height=75px src="../_assets/oslandia_logo.svg"/>
      </td>
      <td>
        <img height=75px src="../_assets/geotribu_carre.png"/>
    </td>
  </tr>
</table>

GitHub: [@guts](https://github.com/guts/)  
GitLab (SaaS): [@geojulien](https://gitlab.com/geojulien/)  
Twitter: [@geojulien](https://twitter.com/geojulien/)

NOTE:

Hello everyone, welcome. I'm Julien, a French GIS consultant in love with open source geospatial for... too long now.  
By day I work at Oslandia, at night I put on my volunteer costume to animate the French-speaking site Geotribu.

---

## Statement

- a plugin still can make QGIS crash (and my core commiters colleagues grumble), so we need to care about plugins quality also
- we spent too much time to maintain and publish a plugin
- we are developers, we should automate our workflows
- we are a team of developers, let's have a talk about our common guidelines and expectations

NOTE:

Developing a plugin for QGIS is both as simple as one of the countless tutorials and as complicated as a software engineering job facing with the dynamism of the project (maintenance requirements), the size of the APIs and constraints that need to be taken into account (Windows, etc.).

At Oslandia, we create and maintain many plugins for our clients, which leads us to streamline their development... and especially their maintenance! Historically, many extensions were created using the amazing Plugin Builder and the underlying tool (pb_tool) but it no longer fit our needs.

We present here our [QGIS Plugin Templater](https://oslandia.gitlab.io/qgis/template-qgis-plugin/), based on [Cookiecutter](https://cookiecutter.readthedocs.io/) and the related work on developer tools (tests, documentation, code structure, formatting, linter...). We will also mention the other tools we are using or following closely (the 3Liz toolbelt, the other template from Gispo Coding...).

Yet Another Plugin Generator? Probably but we think it's worth it!

---

## Goals

- Standardize our (numerous) plugins
- Rationalize support and maintenance
- Use modern tools (CI/CD)
- Keep up to date on ~~best~~ good practices

---

## State of the art

> Some well-known tools to get started with a QGIS plugin boilerplate that we've considered.

---

### The minimal plugin

> The purest.

- it remembers us that a plugin can fit into 3 files
- too much work and time to add industry-ready needs: tests, UI logic, translations, etc.
- such a space of liberty that it favors heterogeneity

[![QGIS Minimal Plugin](../_assets/2022_FOSS4G_Firenze/qgis_minimal_plugin.png)](https://github.com/wonder-sk/qgis-minimal-plugin)

---

### Plugin Builder

> Obviously the most known and used.

- historical with a bunch of resources to use it
- really convenient for beginners, POC, tutorials, teaching...
- opinionated
- carries too much things
- not so cross-platform

[![QGIS Plugin Builder](../_assets/2022_FOSS4G_Firenze/plugin_builder_wizard_required_info.png)](https://g-sherman.github.io/Qgis-Plugin-Builder/)

NOTE:

Also, it's so established that it seems hard to influence it and to make it fitting our specific needs or choices.
Great to:

- start a plugin without opening QGIS
- developers working only on Linux

Down:

- the configuration based logic implies to learn a a micro-framework: "`pb_tool.cfg` is my lord."
- not really cross platform

---

### pb_tool, the Plugin Builder as CLI

> The most `.cfg` oriented tool.

The plugin builder underlying cli:

```bash
python3 -m pip install -U pb_tool

# create the minimal plugin
pb_tool minimal

# create a plugin
pb_tool create
```

NOTE:

It's the tool underlying the Plugin Builder to create skeleton plugins from the command-line.
Great to:

- start a plugin without opening QGIS
- developers working only on Linux

Down:

- the configuration based logic implies to learn a a micro-framework: "`pb_tool.cfg` is my lord."
- not really cross platform

---

## Our decision

> The most gourmet tool 👨‍🍳

A cookiecutter template.

![Cookiecutter for QGIS logo](../_assets/2022_FOSS4G_Firenze/cookiecutter_qgis_logo_128x128.png)

Check-it out: <https://oslandia.gitlab.io/qgis/template-qgis-plugin/>

---

### Why

- Cookiecutter is a well installed tool
- based on robust libraries (Jinja2 for the template engine)
- developer oriented
- flexible
- Python

---

### How to make it

A `cookiecutter.json` files describes available options:

```json
{
  "plugin_name": "My Awesome Plugin",
  "plugin_name_slug": "{{ cookiecutter.plugin_name | slugify | replace('-', '_') }}",
  "plugin_name_class": "{{ cookiecutter.plugin_name_slug.title() | replace('_', '') }}",
  "plugin_category": [
    "Database",
    "Filter",
    "Raster",
    "Vector",
    "Web",
    "None"
  ],
  "plugin_processing": ["yes", "no"],
  "plugin_description_short": "This plugin is a revolution!",
  "plugin_description_long": "Extends QGIS with revolutionary features that every single GIS end-users was expected (or not)!",
  "plugin_tags": "topic1,topic2",
  "plugin_icon": "path_to_an_image_or_leave_blank_to_use_default_icon",
  "author_name": "Firstname LASTNAME",
  "author_org": "Company",
  "author_email": "qgis@company.com",
  "qgis_version_min": "3.22",
  "qgis_version_max": "3.99",
  "repository_url_base": "https://gitlab.com/{{ cookiecutter.author_org }}/{{ cookiecutter.plugin_name_slug }}/",
  "open_source_license": [
    "GPLv2+",
    "GPLv3",
    "MIT",
    "None"
  ],
  "linter_py": [
    "Flake8",
    "PyLint",
    "both",
    "None"
  ],
  "ci_cd_tool": [
    "GitHub",
    "GitLab",
    "None"
  ],
  "ide": [
    "VSCode",
    "None"
  ],
  "post_install_venv": ["no", "yes"],
  "post_git_init": ["no", "yes"],
  "debug": ["no", "yes"],
  "_extensions": ["jinja2_time.TimeExtension"]
}
```

---

### Templating files

Using Jinja2 syntax to generate project replacing values and include some smart logic:

Example with our `plugin_{{cookiecutter.plugin_name_slug}}/{{cookiecutter.plugin_name_slug}}/plugin_main.py`:

```python
[...]
class {{ cookiecutter.plugin_name_class }}Plugin:
    def __init__(self, iface: QgisInterface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class which \
        provides the hook by which you can manipulate the QGIS application at run time.
        :type iface: QgsInterface
        """
        self.iface = iface
        self.log = PlgLogger().log
        {% if cookiecutter.plugin_processing == "yes" %}self.provider = None{% endif %}

        # translation
        plg_translation_mngr = PlgTranslator()
        translator = plg_translation_mngr.get_translator()
        if translator:
            QCoreApplication.installTranslator(translator)
        self.tr = plg_translation_mngr.tr
[...]
```

---

### Hooks

It's possible to run Python code before and after project generation. Typically:

- validating answers
- handle some special cases
- clean up some files

Example `hooks/pre_gen_project.py`:

```python
import re
import sys

MODULE_REGEX = r'^[_a-zA-Z][_a-zA-Z0-9]+$'

plugin_name = '{{ cookiecutter.plugin_name }}'

if not re.match(MODULE_REGEX, plugin_name):
    print('ERROR: %s is not a valid Python module name!' % plugin_name)

    # exits with status 1 to indicate failure
    sys.exit(1)
```

---

## How to use it

---

### Install Cookiecutter

```sh
# typically on Ubuntu
python3 -m pip install --upgrade cookiecutter
# or on Windows
py -3 -m pip install --upgrade cookiecutter
```

![Cookiecutter logo](../_assets/2022_FOSS4G_Firenze/cookiecutter_rectangle.png)

---

### Run and answer questions

Run cookiecutter with our template URL:

```sh
# run cookiecutter with this repository
> cookiecutter https://gitlab.com/Oslandia/qgis/template-qgis-plugin
# if you don't have installed Git, use the ZIP URL
# > cookiecutter https://gitlab.com/Oslandia/qgis/template-qgis-plugin/-/archive/master/template-qgis-plugin-master.zip
```

Answer prompt questions:

```sh
You've downloaded /home/jmo/.cookiecutters/template-qgis-plugin before. Is it okay to delete and re-download it? [yes]: 
plugin_name [My Awesome Plugin]: 
plugin_name_slug [my_awesome_plugin]: 
plugin_name_class [MyAwesomePlugin]: 
Select plugin_category:
1 - Vector
2 - Database
3 - Filter
4 - Raster
5 - Web
6 - None
Choose from 1, 2, 3, 4, 5, 6 [1]: 
Select plugin_processing:
1 - yes
2 - no
Choose from 1, 2 [1]: 
[...]
```

---

### Don't like questions?

Avoid question and/or pass your answer directly to the CLI:

```sh
> cookiecutter --no-input https://gitlab.com/Oslandia/qgis/template-qgis-plugin plugin_name=FOSS4G2022
[2022-08-26 15:25:19,173] [INFO] - GitLab CI removed.
[2022-08-26 15:25:19,174] [INFO] - PyLint configuration removed.
[2022-08-26 15:25:19,174] [INFO] - Plugin foss4g2022 initialized: /tmp/plugin_foss4g2022. Keep up the good work!
```

Or set your default options in a configuration `~/.cookiecutterrc`:

```yml
default_context:
    author_email: julien.moura@oslandia.com
    author_name: Julien Moura
    author_org: Oslandia
    open_source_license: GPLv2+
    plugin_category: Vector
    qgis_version_min: "3.22"
```

---

### Demo

![QGIS Plugin Templater - Démonstration](../_assets/2022_FOSS4G_Firenze/qgis_plugin_templater_init.gif)

---

## What you get?

It depends on your answers 😉!  
Let's take a tour at the defaults.

---

### A settings form and logic

- integrated within the QGIS options menu
- accessible with a menu link
- with a debug mode
- using Python DataClass under the hood for the logic

![QGIS Plugin Templater - QGIS settings integration](../_assets/2022_FOSS4G_Firenze/templater_plugin_settings.png)

---

### A documentation setup

- Sphinx (Markdown and/or RST)
- with the home and setup pages already included
- with CI/CD workflows to build and deploy on GitLab/GitHub Pages

![QGIS Plugin Templater - Documentation](../_assets/2022_FOSS4G_Firenze/templater_result_documentation.png)

---

### Quality dev tools

- a fine .gitignore
- Python formatter: black
- Python linter: PyLint and/or Flake8 (with the great [flake8-qgis](https://github.com/GispoCoding/flake8-qgis))
- git hooks (through pre-commit)
- a changelog (on which the process is based the release workflow)

```sh
> pre-commit run -a
check for added large files..............................................Passed
check for case conflicts.................................................Passed
check xml............................................(no files to check)Skipped
check yaml...............................................................Passed
detect private key.......................................................Passed
fix end of files.........................................................Passed
fix utf-8 byte order marker..............................................Passed
fix python encoding pragma...............................................Passed
trim trailing whitespace.................................................Passed
black....................................................................Passed
isort....................................................................Passed
flake8...................................................................Passed
```

---

### Unit tests

> not so much, just to give you a start

- based on Pytest (soon with [pytest-qgis](https://github.com/GispoCoding/pytest-qgis) 👋 GispoCoding)
- one to test code **independent** from (Py)QGIS
- one to test code **requiring** (Py)QGIS

---

### Modern tooling configuration

> It's time to let pb_tool.cfg take its well-deserved retirement

It pre-configures the wonderful [QGIS Plugin CI](https://opengisch.github.io/qgis-plugin-ci/) library (👋 OpenGIS & 3Liz) to automate:

- manage Qt resources compiling (.qrc file) and integrates with Transifex
- plugin's packaging for making it easy to test for end-users:
  - a downloadable zipped plugin for every MR
  - a custom repository to set up on QGIS to test every version from commit on main branch
- release QGIS official repository for every `git tag`

---

### CI/CD configurations

> Saving you from YAML madness.

- configurations for GitHub Actions and GitLab CI
- running linter and unit tests
- packaging and releasing
- compile and package translations (`.ts` + `.pro` = `.qm`)
- jobs to deploy the documentation to the Git* Pages as a static website...
- ...including a custom plugin repository to deliver your packaged plugin from the git main branch

---

#### GitLab example

> For a release.

![QGIS Plugin Templater - GitLab CI](../_assets/2022_FOSS4G_Firenze/templater_ci_gitlab.png)

---

#### GitHub example

> For a release.

![QGIS Plugin Templater - GitHub CI](../_assets/2022_FOSS4G_Firenze/templater_ci_github.png)

---

### Little extras

- issues and PR/MR templates
- IDE configuration (VS Code only for now)

---

## How to test the result

---

1. Add the custom repository to QGIS:

![QGIS plugin templater repository](../_assets/2022_FOSS4G_Firenze/templater_plugin_qgis_custom_repository.png "QGIS custom plugin repository")

---

2. Install the sample plugin generated with default options:

![QGIS Extensions Manager - Install templater](../_assets/2022_FOSS4G_Firenze/templater_plugin_test_qgis_install.png)

---

## Assessment after 1 year of use at Oslandia

> All our new plugins have been using it.

- speeded up our plugin development, especially to bootstrap a new one (not convenient to upgrade previous plugins)
- reduced the maintenance, raising up the 0-day quality of our plugins and making all plugins similar
- opened some discussions about tech choices (we are developers...)

So: goals achieved!

---

## Conclusion

- definitely not an option for a really beginner in plugins development because it carries a lot of technical choices that can be hard to assimilate
- templating logic is a good option to rationalize development a an industry level production

Most important: this is **A** template, **not THE** template for a QGIS plugin. If it doesn't fit your needs, make your own (and share it obviously)!

For example, check out this another template by Gispo Coding <https://github.com/GispoCoding/cookiecutter-qgis-plugin/>.

---

## Thanks for your attention

> Please, feel free to ask your questions

Happy developing plugins for QGIS!

See you on exhibition area or later [Oslandia](https://oslandia.com).
