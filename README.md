# Talks and workshops by Oslandia

All public presentations done by Oslandia.

Shield: [![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]

This work is licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa].

[![CC BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://licensebuttons.net/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg

## Installation

### Prerequisites

- Node LTS
- or Docker

### Slides

```sh
npm install --include=dev
```

## Serve Content Locally (with Automatic Reloading)

```sh
npm run serve
```

With Docker:

```sh
docker run --rm -p 1948:1948 -p 35729:35729 -v .:/slides webpronl/reveal-md:latest /slides --css _assets/oslandia.css --watch
```

The slides are viewable at: <http://localhost:1948/>.

## Generate Slides

```sh
npm run publish-whole
```
