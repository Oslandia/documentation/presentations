---
title: "Conf QGIS FR 2024 - Atelier Initiation au déploiement rationalisé de QGIS avec PowerShell et QDT"
tags:
  - atelier
  - communication
  - déploiement
  - journées QGIS FR
  - Powershell
  - QDT
  - QGIS Deployment Toolbelt
type: slide
slideOptions:
  theme: oslandia
  transition: 'fade'
---

## Initiation au déploiement rationalisé de QGIS

![Affiche conf Grenoble](../../_assets/conf_qgis_fr_2024_atelier_powershell_qdt/conf_qgis_fr_2024_affiche.png)

---

## Qui vous parle

Julien [![logo Mastodon](../../_assets/mastodon.png)](https://mapstodon.space/@geojulien) [![logo Geotribu](../../_assets/geotribu.png)](https://geotribu.fr/team/julien-moura/) - Florent : [![logo X (Twitter)](../../_assets/X_logo_2023.png)](https://twitter.com/florent1foug)

![Bannière LinkedIn](../../_assets/conf_qgis_fr_2024_atelier_powershell_qdt/banner_linkedin.jpeg)

---

## Plan de l'atelier

- ♨️ Rappels de base
- ♻️ Cycle de vie du projet et installeurs QGIS
- 🐚 Scripter l'installation
- 🧰 Utiliser QDT
- 👩‍🍳 Concocter vos propres profils

---

## Prérequis

- une connexion internet ouverte sur osgeo.org, qgis.org, gitlab.com, github.com et pypi.org
- PowerShell >= 5.1
- Python >= 3.10

---

### Pourquoi que Windows ?

![Mème Bill Gates](../../_assets/conf_qgis_fr_2024_atelier_powershell_qdt/meme_microsoft_bill_gates.png)

---

#### Parce-que

[![Dashboard QGIS sur les plateformes les plus utilisées](../../_assets/conf_qgis_fr_2024_atelier_powershell_qdt/qgis_dashboard_top100_operating-system.webp)](https://feed.qgis.org/metabase/public/dashboard/df81071d-4c75-45b8-a698-97b8649d7228)

---

## Séparation des pouvoirs

![Séparation des pouvoirs, parodie Montesqieu](../../_assets/conf_qgis_fr_2024_atelier_powershell_qdt/Montesquieu_separation_pouvoirs.png)

---

### Logiciel

![Installer QGIS](../../_assets/conf_qgis_fr_2024_atelier_powershell_qdt/installation-qgis_dall-e.webp)

---

### Configuration

![Console lumière](../../_assets/conf_qgis_fr_2024_atelier_powershell_qdt/configuration_console-lumiere-dmx-24.webp)

---

### Personnalisation

![Personnalisation avec les profils](../../_assets/conf_qgis_fr_2024_atelier_powershell_qdt/personnalisation_profils.png)

---

## Le cycle de vie du projet QGIS

![Cycle de vie des versions de QGIS](../../_assets/conf_qgis_fr_2024_atelier_powershell_qdt/qgis_life_cycle.png)

----

### À retenir

- Calendrier fixe de 4 mois
- Version paires = versions publiées (3.12, 3.14, 3.16, 3.18, 3.20, 3.22, ...)
- Versions impaires = versions de développement (3.13, 3.15, 3.17, 3.19, 3.21...)
- LTR = Long Term Release. Correctifs pendant 1w an - soit 12 versions
- Release = 4 mois / 4 cycles correctifs.
- Backport = opération de portage d'un correctif vers les versions maintenues
release et ltr

---

## LTR = sécurité

![GIF LTR](https://geotribu.fr/assets/external/media.giphy.com/media/nneVpy2YnHZNm/giphy.gif)

Note:

Rare image d'une géomaticienne travaillant sur unprojet cartographique critique avec une version impaire de QGIS

---

## Les installeurs

![Page de téléchargement du site de QGIS](../../_assets/conf_qgis_fr_2024_atelier_powershell_qdt/qgis_download_page_installer.png)

---

### OSGeo4W

![Voiture tunée OSGeo4W](https://cdn.geotribu.fr/img/articles-blog-rdp/articles/qgis_customize_osgeo4w_rha/qgis_osgeo4w_voiture_rallye.png)

----

### OSGeo4W : les **+**

- optimisation du réseau avec des mises à jour différentielles
- à la base du MSI officiel

----

### OSGeo4W : les **-**

- projet open source mais quasi non communautaire, peu documenté et soumis à des changements imprévisibles
- maintenance complexe
- [bus factor](https://fr.wikipedia.org/wiki/Facteur_d%27autobus) de 1
- greffe unixienne
- mauvaise intégration à l'écosystème Windows
- principes de sécurité discutables

---

### Package MSI

![Icône format MSI](../../_assets/conf_qgis_fr_2024_atelier_powershell_qdt/logo_msi_package.png)

---

## Automatiser le déploiement de QGIS

🎯 Objectifs 🎯

- connaître son parc QGIS
- rendre prévisible la disponibilité du logiciel
- réduire le temps de support
- consolider l'expérience des utilisateur/ices finaux

---

### Les grandes étapes

1. télécharger l'installeur
1. installer une nouvelle version de QGIS
1. désinstaller l'ancienne version de QGIS

---

### Le pouvoir des coquillages

![Boticelli le pouvoir des coquillages](../../_assets/conf_qgis_fr_2024_atelier_powershell_qdt/Boticelli_powershell.jpeg)

---

### Télécharger et installer QGIS avec PowerShell

![À vous de jouer](../../_assets/conf_qgis_fr_2024_atelier_powershell_qdt/a-vous-de-jouer.webp)

----

#### TP

🎯 Objectifs 🎯

- Scripter le téléchargement du MSI
- Scripter l'installation
- Scripter la désinstallation de QGIS

---

### Télécharger

```powershell
Invoke-WebRequest -Uri "$QGIS_MSI_SOURCE$QGIS_MSI_LATEST.msi" -OutFile $OutMsi
Invoke-WebRequest -Uri "$QGIS_MSI_SOURCE$QGIS_MSI_LATEST.sha256sum" -OutFile $OutSum
```

---

### Installer

```powershell
Start-Process msiexec.exe -Wait -Passthru `
    -ArgumentList "/I $QGIS_MSI_INSTALL_FILE /L*v $QGIS_MSI_PATH_LOGS\qgis-install.log INSTALLDIR=" `
    "$QGIS_MSI_PATH_INSTALLATION"" /qn"
```

---

### Désinstaller

```powershell
Start-Process msiexec.exe -Wait -PassThru `
  -ArgumentList "/X $QGIS_MSI_UNINSTALL_FILE /L*v $QGIS_MSI_LOGS_PATH\qgis-uninstall.log /qn"
```

---

## Gestion des profils avec QGIS Deployment Toolbelt

QDT :

- outil en ligne de commande, disponible également en exécutable _one-click run_
- une méthodologie et un outil pour rationnaliser la gestion des profils QGIS
- **pas** un outil d'installation de QGIS

![logo QDT](../../_assets/conf_qgis_fr_2024_atelier_powershell_qdt/logo_qdt.png)

![Aide du CLI QDT](../../_assets/conf_qgis_fr_2024_atelier_powershell_qdt/qdt_help.png)

---

## Lancer QDT

![À vous de jouer](../../_assets/conf_qgis_fr_2024_atelier_powershell_qdt/a-vous-de-jouer.webp)

----

### TP

🎯 Objectifs 🎯

- déployer les profils préparés pour la conférence

📚 Voir le dépôt préparé pour l'atelier : <https://gitlab.com/Oslandia/qgis/profils_qgis_fr>

```powershell
python3 -m pip install -U pip setuptools wheel
python3 -m pip install -U qgis-deployment-toolbelt
```

---

## Concepts QDT

- descripteurs de profils QGIS
- scénario de déploiement

![icone profile](../../_assets/conf_qgis_fr_2024_atelier_powershell_qdt/icon_profiles.png) ![icone scénario](../../_assets/conf_qgis_fr_2024_atelier_powershell_qdt/icon_scenario.png)
